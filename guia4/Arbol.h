#include <iostream>
using namespace std;

//Definimos la estructura Nodo. (En arbol.h, porque los nodos están en el arbol.
typedef struct _Nodo {
    int dato;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;


class Arbol {
	public:
		Arbol();
		Nodo *crearNodo(int);
		void insertarNodo(Nodo *&, int);
		void imprimirPreorden(Nodo *);
		void imprimirInorden(Nodo *);
		void imprimirPosorden(Nodo *);
		void eliminarNodo(Nodo *&, int);

      
};


