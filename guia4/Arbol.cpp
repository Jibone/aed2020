#include <iostream>
#include "Grafo.h"
//#include "Arbol.h"

//Constructor vacío
Arbol::Arbol(){
}

//Metodo para crear un nuevo nodo con un dato del tipo entero en el.
Nodo* Arbol::crearNodo(int n){
	Nodo *nuevoNodo = new Nodo();
	
	nuevoNodo->dato = n;
	nuevoNodo->der = NULL;
	nuevoNodo->izq = NULL;
	
	return nuevoNodo;
}

/*Con este metodo ingresamos en el arbol nodos creados con crearNodo(), si no había nodos antes se convierte en la raiz
pero de haber mas nodos va a ir comparandose con estos para ver a qué lado del arbol debe ir ordenandose el nuevo nodo. */
void Arbol::insertarNodo(Nodo *&arbol, int n){
	if(arbol == NULL){
		Nodo *nuevoNodo = crearNodo(n);
		arbol = nuevoNodo;
	}else{
		int valorRaiz = arbol->dato;
		
		if(n < valorRaiz){
			insertarNodo(arbol->izq, n);
		}
		else if(n > valorRaiz){
			insertarNodo(arbol->der, n);		
		}else{
			cout << "¡Error! -Dato Ya Existente-" << endl;
		}
			
	}
}

//Metodos para la impresión del arbol.
void Arbol::imprimirPreorden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		cout << arbol->dato << "-";
		imprimirPreorden(arbol->izq);
		imprimirPreorden(arbol->der);
	}
}
void Arbol::imprimirInorden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		imprimirInorden(arbol->izq);
		cout << arbol->dato << "-";
		imprimirInorden(arbol->der);
	}
}
void Arbol::imprimirPosorden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		imprimirPosorden(arbol->izq);
		imprimirPosorden(arbol->der);
		cout<< arbol->dato << "-";
	}
}

//Metodo para la eliminación del nodo con el dato ingresado, se hace utilizacion de nodos auxiliares para reordenar el arbol posteriormente
void Arbol::eliminarNodo(Nodo *&nEliminar, int n){
	
	Nodo* nOtro = NULL;
	Nodo* nAux_der = NULL;
	Nodo* nAux_derMax = NULL;
	
	if(nEliminar != NULL){
		if(n < nEliminar->dato){
			eliminarNodo(nEliminar->izq, n);
		}
		else if(n > nEliminar->dato){
			eliminarNodo(nEliminar->der, n);
		}else{
			nOtro = nEliminar;
			if(nOtro->der == NULL){
				nEliminar = nOtro->izq;
			}
			else if(nOtro->izq == NULL){
				nEliminar = nOtro->der;
			}
			else{
				nAux_der = nEliminar->der;
				nEliminar = nOtro->izq;
				delete nOtro;
				nAux_derMax = nEliminar;
				
				while(nAux_derMax->der != NULL){
					nAux_derMax = nAux_derMax->der; 
				}
				nAux_derMax->der = nAux_der;
			}
		}
	}
	else{
		cout<< "Nodo no encontrado." <<endl;
	}
}

