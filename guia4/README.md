#Creador de Arboles Binarios.
El programa sirve para crear un arbol binario ordenado con los valores ingresados por el usuario, además, los nodos del arbol pueden
ser modificados (en su valor) o si bien se desea, eliminados, igualmente después de estas operaciones el arbol seguirá manteniendo 
su orden. Este arbol puede mostrar por pantalla sus datos en 3 tipos de orden: Preorden, inorden y posorden. Y finalmente (se puede hacer 
esto cuando se quiera sin problemas la verdad) puede convertir la información del arbol en un Grafo para poder Visualizarlo con una
interpretación mas gráfica.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez ejecutado el programa este desplegará un menú en pantalla, pidiéndole al usuario ingresar el numero de la opción deseada a traves
de la terminal. Y si en algun momento se desea cerrar el programa basta con escoger esa opción entre las que te ofrece el menu.
El programa está desarrollado en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.


