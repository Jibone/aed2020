#include <iostream>
#include <fstream>
#include "Arbol.h"

using namespace std;


class Grafo {
    private:
        Nodo *arbol = NULL;

    public:
        /* constructor*/
        Grafo(Nodo *arbol);
        
        /* Métodos de la clase Grafo */
        void crearGrafo();
        void recorrerArbol (Nodo *, ofstream &);
        
};
