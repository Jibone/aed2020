# Creador de Listas Ordenadas.
Este programa genera dos lista con valores ingresados por el usuario, los cuales a medida que son ingresados van a ir siendo ordenados de 
forma creciente y mostrados en pantalla. Una vez que el usuario lo decida puede mezclar estas dos listas para formar una tercera lista
la cual tendrá los datos de las 2 listas y ordenados posterior a su mezcla. Finalmente serán mostradas en pantalla las listas 1, 2 y 3.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez ejecutado el programa este desplegará un menú en pantalla, pidiéndole al usuario ingresar una opción, la cual sería
el número correspondiente a la opción deseada. El programa es bastante intuitivo por lo que no se deberían tener mayores complicaciones.
El programa está desarrollado en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.


