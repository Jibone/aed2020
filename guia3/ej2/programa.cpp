#include <iostream>
#include "Lista.h"
using namespace std;

//Creamos una funcion menu() para ahorrar espacio en el main.
void menu(){
	cout << " " << endl;
	cout << "-----------Menu------------" << endl;
	cout << "Ingresar dato en lista 1 [1]" << endl;
	cout << "Ingresar dato en lista 2 [2]" << endl;
	cout << "Mezclar listas           [3]" << endl;
	cout << "---------------------------" << endl;
}
int main(){
	bool ciclo = true;
	int ingreso;
	int opcion;
	//Creamos las lista listas
	Lista lista1 = Lista();
	Lista lista2 = Lista();
	Lista lista3 = Lista();
	cout << "------Bienvenido al Creador y Mezclador de Listas------" << endl;
	//Este ciclo funcionará hasta que se decida mezclar las listas.
	while(ciclo == true){
		menu();
		cout << "Ingrese su opción: ";
		cin >> opcion;
		switch(opcion){
			case 1: 
				cout << "Ingrese un numero entero: ";
				cin >> ingreso;
				lista1.crear(ingreso);
				lista1.ordena_lista();
				cout << "Vista actual de la lista 1: " << endl;
				lista1.imprimir();
				break;
			case 2:
				cout << "Ingrese un numero entero: ";
				cin >> ingreso;
				lista2.crear(ingreso);
				lista2.ordena_lista();
				cout << "Vista actual de la lista 2: " << endl;
				lista2.imprimir();
				break;
			case 3:
				cout << "Vista actual de la lista 1: " << endl;
				lista1.imprimir();
				cout << "Vista actual de la lista 2: " << endl;
				lista2.imprimir();
				lista3.mezclar_listas(lista1, lista2);
				lista3.ordena_lista();
				cout << "Vista actual de la lista 3: " << endl;
				lista3.imprimir();
				ciclo = false;
				break;	
		}
	}	
	return 0;
}
