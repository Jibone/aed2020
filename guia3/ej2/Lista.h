#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H
//Se introduce la estructura de Nodo en este .h ya que tenía bastante sentido, ya que son las listas las que tienen nodos.
typedef struct _Nodo{
	int num;
	struct _Nodo *sig;
} Nodo;

//Se crea la clase lista con sus atributos y métodos.
class Lista {
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
		/* Constructor*/
		Lista();
		Nodo* get_raiz();
		Nodo* get_ultimo();
		//Este metodo mezcla 2 listas ya rellenadas, y genera una nueva con esos datos.
		void mezclar_listas(Lista lista1, Lista lista2); 
		/* Este método crea un nuevo nodo y recibe como parámetro un entero que será atributo en dicho nodo.*/
		void crear (int n);
		/* Método que ordena la lista de menor a mayor */
		void ordena_lista();
		/*  Método que imprime la lista */
		void imprimir ();
};
#endif
