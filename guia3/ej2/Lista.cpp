#include <iostream>
#include "Lista.h"
using namespace std;

//Aquí tenemos un constructor simple.
Lista::Lista() {}

//Con este metodo retornamos un puntero que apunta a la raiz de esta lista.
Nodo* Lista::get_raiz(){
	return this->raiz;
}
//Con este metodo retornamos un puntero que apunta al ultimo nodo de esta lista.
Nodo* Lista::get_ultimo(){
	return this->ultimo;
}
/* Con este metodo mezclaremos las listas pasadas como parametro
 Para esto usaremos los metodos get_raiz() y get_ultimo(). Haremos que la raíz de la lista1 sea la nueva
 raiz de la lista3, conectaremos el final de la lista1 con el principio de la lista2, y convertiremos el final de la lista2
 en el final de la lista3. En el main, posterior a esto, la lista3 será ordenada con ordenar_lista(). */
void Lista::mezclar_listas(Lista lista1, Lista lista2){
	Nodo *raiz1;
	Nodo *ultimo1;
	Nodo *raiz2;
	Nodo *ultimo2;
	raiz1 = lista1.get_raiz();
	ultimo1 = lista1.get_ultimo();
	raiz2 = lista2.get_raiz();
	ultimo2 = lista2.get_ultimo();
	this->raiz = raiz1;
	ultimo1->sig = raiz2;
	this->ultimo = ultimo2;
}
//Con este metodo se comienza a crear la lista.
void Lista::crear (int n) {
	Nodo *tmp;
	//crea un nodo
	tmp = new Nodo;
	//guarda en numero
	tmp->num = n;
	//crea el puntero al siguiente nodo como un NULL, indicando que es el ultimo
	tmp->sig = NULL;

	//si es primer nodo de la lista, lo deja como raíz y como último nodo
	if (this->raiz == NULL) 
	{ 
		this->raiz = tmp;
		this->ultimo = this->raiz;
	//de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
	}
	else
	{
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
}

/*Con este metodo se ordena la lista, utilizando un nodo para recorrer la lista, y un auxiliar llamado valor
que nos servirá para ir moviendo de nodo los valores*/
void Lista::ordena_lista() {
	Nodo *tmp = this->raiz;
	int valor;
	int contador = 0;
	while (tmp){           //Mientras tmp exista (!= NULL) este while seguirá en funcionamiento
		tmp = tmp->sig;
		contador++;
	}
	tmp = this->raiz;
	
	for (int j=0; j < contador; j++)
	{
		while (tmp->sig)  //Mientra exista (!= NULL) tmp->sig va a estar en funcionamiento este while
		{
			if (tmp->num > tmp->sig->num)
			{
				valor = tmp->num;
				tmp->num = tmp->sig->num;
				tmp->sig->num = valor;

				tmp = tmp->sig;//Tmp pasa a ser el nodo siguiente
			}
			else{
				tmp = tmp->sig;//Tmp pasa a ser el nodo siguiente
			}
		}	
		tmp = this->raiz;//tmp vuelve a ser this->raiz
	}
}

//Con este metodo se se imprime por terminal la lista.
void Lista::imprimir () {  
	//Se crea un nodo tmp para ir recorriendo la lista.
	Nodo *tmp = this->raiz;
	int cuenta = 1;

	while (tmp != NULL) {
		cout << cuenta << ") " << tmp->num << endl; //Aquí va imprimiendo.
		tmp = tmp->sig;
		cuenta = cuenta + 1;
	}
}
