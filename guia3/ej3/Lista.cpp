#include <iostream>
#include "Lista.h"
using namespace std;

//Aquí tenemos un constructor simple.
Lista::Lista() {}


/* Con este metodo recorreremos la lista, y si detecta que entre los valores de un nodo y el siguiente
	hay más de 1 de diferencia, ingresará un nuevo nodo que se posicionará en ese lugar, rellenando la lista y de haber sido 
	ingresados datos repetidos estos aun se conservarán al rellenar la lista */

void Lista::rellenar_lista(){
	Nodo *tmp = this->raiz;

	while (tmp != NULL) {
		if(tmp->sig != NULL && (tmp->sig->num - tmp->num) > 1){
			crear(tmp->num + 1);
			ordena_lista();
		}
	tmp = tmp->sig;
	}
}
//Con este metodo se comienza a crear la lista.
void Lista::crear (int n) {
	Nodo *tmp;
	//crea un nodo
	tmp = new Nodo;
	//guarda en numero
	tmp->num = n;
	//crea el puntero al siguiente nodo como un NULL, indicando que es el ultimo
	tmp->sig = NULL;

	//si es primer nodo de la lista, lo deja como raíz y como último nodo
	if (this->raiz == NULL) 
	{ 
		this->raiz = tmp;
		this->ultimo = this->raiz;
	//de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
	}
	else
	{
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
}

/*Con este metodo se ordena la lista, utilizando un nodo para recorrer la lista, y un auxiliar llamado valor
que nos servirá para ir moviendo de nodo los valores*/
void Lista::ordena_lista() {
	Nodo *tmp = this->raiz;
	int valor;
	int contador = 0;
	while (tmp){           //Mientras tmp exista (!= NULL) este while seguirá en funcionamiento
		tmp = tmp->sig;
		contador++;
	}
	tmp = this->raiz;
	
	for (int j=0; j < contador; j++)
	{
		while (tmp->sig)  //Mientra exista (!= NULL) tmp->sig va a estar en funcionamiento este while
		{
			if (tmp->num > tmp->sig->num)
			{
				valor = tmp->num;
				tmp->num = tmp->sig->num;
				tmp->sig->num = valor;

				tmp = tmp->sig;//Tmp pasa a ser el nodo siguiente
			}
			else{
				tmp = tmp->sig;//Tmp pasa a ser el nodo siguiente
			}
		}	
		tmp = this->raiz;//tmp vuelve a ser this->raiz
	}
}

//Con este metodo se se imprime por terminal la lista.
void Lista::imprimir () {  
	//Se crea un nodo tmp para ir recorriendo la lista.
	Nodo *tmp = this->raiz;
	int cuenta = 1;

	while (tmp != NULL) {
		cout << cuenta << ") " << tmp->num << endl; //Aquí va imprimiendo.
		tmp = tmp->sig;
		cuenta = cuenta + 1;
	}
}
