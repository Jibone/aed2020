#include <iostream>
#include "Lista.h"
using namespace std;

//Creamos una funcion menu() para ahorrar espacio en el main.
void menu(){
	cout << " " << endl;
	cout << "-----------Menu------------" << endl;
	cout << "Ingresar dato en la lista [1]" << endl;
	cout << "Relleno automático        [2]" << endl;
	cout << "---------------------------" << endl;
}
int main(){
	//Declaramos estas primeras 3 variables sirven para el funcionamiento de lo que es el menú del programa.
	bool ciclo = true;
	int ingreso;
	int opcion;
	//Creamos las lista listas
	Lista lista = Lista();
	cout << "------Bienvenido al Creador y Rellenador de Listas------" << endl;
	//Este ciclo funcionará hasta que se decida mezclar las listas.
	while(ciclo == true){
		menu();
		cout << "Ingrese su opción: ";
		cin >> opcion;
		switch(opcion){
			case 1: 
				cout << "Ingrese un numero entero: ";
				cin >> ingreso;
				lista.crear(ingreso);
				lista.ordena_lista();
				cout << "Vista actual de la lista: " << endl;
				lista.imprimir();
				break;
			case 2:
				lista.rellenar_lista();
				cout << "Vista final de la lista rellenada: " << endl;
				lista.imprimir();
				ciclo = false;
				cout << "*Programa Finalizado*" << endl;
				break;
		}
	}	
	return 0;
}
