# Creador de Listas Ordenadas.
Este programa genera una lista con valores ingresados por el usuario, los cuales a medida que son ingresados van a ir siendo ordenados de 
forma creciente y mostrados en pantalla.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez ejecutado el programa este desplegará un menú en pantalla, pidiéndole al usuario ingresar una opción; aquí puede escoger
añadir un dato a la lista, o finalizar el programa.
El programa está desarrollado en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.


