#include <iostream>
#include "Lista.h"
using namespace std;

int main(){
	bool ciclo = true;
	int ingreso;
	int opcion;
	//Creamos la lista
	Lista lista = Lista();
	cout << "------Bienvenido al Creador de Listas Ordenadas------" << endl;
	//Hacemos un ciclo el permitirá ingresar valores nuevos hasta que se decida finalizar el programa ingresando 0.
	while(ciclo == true){
		cout << "Para añadir un nuevo dato ingrese 1, para finalizar el programa ingrese 0: ";
		cin >> opcion;
		switch(opcion){
			case 0: 
				cout << "¡Hasta la próxima!" << endl;
				ciclo = false;
				break;
			case 1:
				cout << "Ingrese un numero entero: ";
				cin >> ingreso;
				lista.crear(ingreso);
				lista.ordena_lista();
				cout << "Vista actual de la lista: " << endl;
				lista.imprimir();
				break;
		}
	}	
	return 0;
}
