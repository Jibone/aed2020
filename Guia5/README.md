#Generador de Graficos De Arboles binarios balanceados de IDs de Proteínas"

El programa busca satisfacer la necesidad de generar un gráfico de arboles binarios balanceados a partir de un archivo.txt 
el cual contendrá la informacion correspondiente para la generación de éste. Las IDs de proteínas que van a ser ingresadas y ordenadas
en él se ingresan de forma automática desde el archivo "ids.txt" que se encuentra en la carpeta del programa.
El programa presenta un menú desde el cual el usuario puede escoger entre varias opciones, tales como: (1)Ingresar nuevos elementos
que entrarán de forma ordenada/balanceada en el arbol bajo un criterio alfanumérico. (2) Buscar un elemento específico en el arbol,
se entregará como respuesta si está o no está en el arbol. (3)Eliminar un elemento del arbol, de forma de seguir mantendiendo ordenados
los demás elementos. (4)Modificar un elemento del arbol por otro nuevo, para esto debe existir el elemento a modificar, y además
el nuevo elemento no debe encontrarse en el arbol al momento de ingresarlo. (5)Generar el gráfico con los elementos
ingresados + las IDs de proteínas.
En un principio se buscaba generar un gráfico que pudiera incluir a muchas mas IDs pero al momento de generarlo no se visualizaba
nada en la imagen, es por esto que se limitó la cantidad de IDs incluidas en el archivo "ids.txt" a 1500. 
Al momento de generar el gráfico este demora en generarse por lo que nos soltará un error al momento de generarlo por primera vez,
sin embargo al esperar un momento y volver a seleccionar la opción de generar el gráfico este abrirá el grafico generado anteriormente.
Para poder ejecutar este programa se debe tener instalado los comandos de make y graphviz en una terminal de linux.
Para iniciar se debe dirigir a la carpeta que contiene los archivos a través de la terminal, compilar el programa con el comando "make"
para despues ejecutar el programa con el comando "./programa".
El programa está escrito en el lenguaje de programación C++.


