#include "Arbol.h"
#include "Grafo.h"
using namespace std;

#define TRUE 1
#define FALSE 0

//Constructor de la clase Arbol
Arbol::Arbol(){

}

//Función que inserta un elemento en el arbol de manera ordenada (lo deja balanceado)
void Arbol::InsercionBalanceado(NODO *&nodo_nuevo, int *BO, string infor) {
	//Nodos auxiliares para la insercion
	NODO *nodo = NULL;
	NODO *nodo1 = NULL;
	NODO *nodo2 = NULL;
	nodo = nodo_nuevo;
	/*Con este metodo ingresamos en el arbol nodos creados con crearNodo(), si no había nodos antes se convierte en la raiz
	pero de haber mas nodos va a ir comparandose con estos para ver a qué lado del arbol debe ir ordenandose el nuevo nodo. */
	//Se comprueba si es que existe el nodo raíz, de no existir este nuevo dato ingresado se convertiría en la raiz del arbol
	if (nodo != NULL) {
		//De haber habido raíz; este nuevo elemento compara sus datos con ella (alfanumericamente) y el valor que entrega esto decidirá hacia que lado se irá
		if (infor.compare(nodo->info) <= -1) {
			/*Esto sucede en el caso de que se vaya por el lado izquierdo se hace uso recursivo de InsercionBalanceado()
			para volver a hacer la comparativa con este nodo y ver si existe o no. */
			InsercionBalanceado(nodo->izq, BO, infor);
			//Se comprueba si el arbol ha crecido
			if(*BO == TRUE) {
				//Dependiendo del factor de equilibrio se balanceará al inserción
				switch (nodo->FE) {
					//El caso de que el factor de equilibrio fuese 1
					case 1:
					//Ahora sería de 0
					nodo->FE = 0;
					*BO = FALSE;
					break;
					//El caso de que el factor de equilibrio fuese 0
					case 0:
					//Ahora sería -1
					nodo->FE = -1;
					break;

					//Si el factor de equilibrio era -1 ahora el arbol debe ser reestructurado
					case -1:
					nodo1 = nodo->izq;
					/* Rotacion II */
					if (nodo1->FE <= 0) {
						nodo->izq = nodo1->der;
						nodo1->der = nodo;
						nodo->FE = 0;
						nodo = nodo1;
					}else{
						/* Rotacion ID */
						nodo2 = nodo1->der;
						nodo->izq = nodo2->der;
						nodo2->der = nodo;
						nodo1->der = nodo2->izq;
						nodo2->izq = nodo1;

						//Se modifica el FE respecto a la rotación si es que tiene elementos en la izquierda
						if (nodo2->FE == -1){
							nodo->FE = 1;
						}else{
							nodo->FE =0;
						}

						//Se modifica el FE respecto a la rotación si es que tiene elementos en la derecha
						if(nodo2->FE == 1){
							nodo1->FE = -1;
						}else{
							nodo1->FE = 0;
						}
						nodo = nodo2;
					}
					nodo->FE = 0;
					*BO = FALSE;
					break;
				}
			}

			//Aquí seguiría en caso de que el elemento nuevo sea mayor al que se compara
		}else{
			if (infor.compare(nodo->info) >= 1){
				//Se inserta recursivamente el elemento nuevo por la derecha
				InsercionBalanceado(nodo->der, BO, infor);

				//Se comprueba si el arbol a crecido
				if(*BO == TRUE) {
				//Dependiendo del factor de equilibrio se balanceará la inserción
					switch (nodo->FE) {
						//Si el FE era -1 ahora será 0
						case -1:
						nodo->FE = 0;
						*BO = FALSE;
						break;
						//Si el FE era 0 ahora será 1
						case 0:
						nodo->FE = 1;
						break;

						//Si el FE era 1 el arbol se debe reestructurar
						case 1:
						nodo1 = nodo->der;

						if (nodo1->FE >= 0) {
							/* Rotacion DD */
							nodo->der = nodo1->izq;
							nodo1->izq = nodo;
							nodo->FE = 0;
							nodo = nodo1;

						}else{
							/* Rotacion DI */
							nodo2 = nodo1->izq;
							nodo->der = nodo2->izq;
							nodo2->izq = nodo;
							nodo1->izq = nodo2->der;
							nodo2->der = nodo1;

							if (nodo2->FE == 1){
							nodo->FE = -1;
							}else{
								nodo->FE = 0;
							}

							if (nodo2->FE == -1){
							nodo1->FE = 1;
							}else{
								nodo1->FE = 0;
							}
							nodo = nodo2;
						}
						nodo->FE = 0;
						*BO = FALSE;
						break;
					}
				}
			}else{
				cout << "El nodo ya se encuentra en el árbol" << endl;
			}
		}
		//En caso de que nodo se NULL
	}else{
		//Se crea el nodo y se le asigna informacion
		nodo = new NODO;
		nodo->izq = NULL;
		nodo->der = NULL;
		nodo->info = infor;
		nodo->FE = 0;
		*BO = TRUE;
	}
	//Se le asignan los valores de nodo a nodo_nuevo
	nodo_nuevo = nodo;
}

//Se busca un nodo determinado en el arbol con recursividad
void Arbol::Busqueda(NODO *nodo, string infor, bool &existe) {
	if (nodo != NULL) {
		if (infor.compare(nodo->info) <= -1){
		//Busca por la izquierda
		Busqueda(nodo->izq, infor, existe);
		}else{
			if (infor.compare(nodo->info) >= 1){
				//Busca por la derecha
				Busqueda(nodo->der,infor, existe);
			}else{
				cout << "El nodo SI se encuentra en el árbol" << endl;
				existe = true;
			}
		}
	}else{
		cout << "El nodo NO se encuentra en el árbol" << endl;
		existe = false;
	}
}

//Reestructuracion del arbol
void Arbol::Restructura1(NODO **nodocabeza, int *BO) {
	NODO *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;

	if (*BO == TRUE) {
		switch (nodo->FE) {
			case -1:
			nodo->FE = 0;
			break;

			case 0:
			nodo->FE = 1;
			*BO = FALSE;
			break;

			case 1:
			//Se balancea el arbol dependiendo de su factor de equilibrio
			nodo1 = nodo->der;

			if (nodo1->FE >= 0) {
				/* rotacion DD */
				nodo->der = nodo1->izq;
				nodo1->izq = nodo;

				switch (nodo1->FE) {
					case 0:
					nodo->FE = 1;
					nodo1->FE = -1;
					*BO = FALSE;
					break;

					case 1:
					nodo->FE = 0;
					nodo1->FE = 0;
					*BO = FALSE;
					break;
				}
				nodo = nodo1;
			}else {
				/* Rotacion DI */
				nodo2 = nodo1->izq;
				nodo->der = nodo2->izq;
				nodo2->izq = nodo;
				nodo1->izq = nodo2->der;
				nodo2->der = nodo1;

				if (nodo2->FE == 1){
					nodo->FE = -1;
				}else{
					nodo->FE = 0;
				}
				
				if (nodo2->FE == -1){
					nodo1->FE = 1;
				}else{
					nodo1->FE = 0;
				}
				nodo = nodo2;
			}
			nodo2->FE = 0;
			*BO = FALSE;
			break;
		}
	}
	*nodocabeza = nodo;
}

//Reestructuracion del arbol
void Arbol::Restructura2(NODO **nodocabeza, int *BO) {
	NODO *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;

	if (*BO == TRUE) {
		switch (nodo->FE) {
			case 1:
			nodo->FE = 0;
			break;

			case 0:
			nodo->FE = -1;
			*BO = FALSE;
			break;

			case -1:
			// se procede a balancear el arbol dependiendo de su factor de equilibrio
			nodo1 = nodo->izq;
			if (nodo1->FE<=0) {
				/* rotacion II */
				nodo->izq = nodo1->der;
				nodo1->der = nodo;
				switch (nodo1->FE) {
					case 0:
					nodo->FE = -1;
					nodo1->FE = 1;
					*BO = FALSE;
					break;

					case -1:
					nodo->FE = 0;
					nodo1->FE = 0;
					*BO = FALSE;
					break;
				}
				nodo = nodo1;
			} else {
				/* Rotacion ID */
				nodo2 = nodo1->der;
				nodo->izq = nodo2->der;
				nodo2->der = nodo;
				nodo1->der = nodo2->izq;
				nodo2->izq = nodo1;

				if (nodo2->FE == -1){
					nodo->FE = 1;
				}	
				else{
					nodo->FE = 0;
				}

				if (nodo2->FE == 1){
					nodo1->FE = -1;
				}
				else{
					nodo1->FE = 0;
				}
				nodo = nodo2;
			}
			nodo->FE = 0;
			*BO = FALSE;
			break;
        }
	}
  	*nodocabeza = nodo;
}

//Metodo para borrar un nodo de forma correcta 
void Arbol::Borra(NODO **aux1, NODO **otro1, int *BO) {
	//Auxiliares
	NODO *aux, *otro;
	aux = *aux1;
	otro = *otro1;

	if (aux->der != NULL) {
		Borra(&(aux->der), &otro, BO);
		Restructura2(&aux, BO);
	} else {
		otro->info = aux->info;
		aux = aux->izq;
		*BO = TRUE;
	}
	*aux1 = aux;
	*otro1 = otro;
}

//Metodo para Eliminar un nodo manteniendo el arbol balanceado
void Arbol::EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor) {
	//Auxiliares
	NODO *nodo, *otro;
	nodo = *nodocabeza;

	//Se realiza un proceso de busqueda muy similar al metodo Busqueda() pero en este caso se elimina un nodo y se reestructura el arbol
	if (nodo != NULL) {
		if (infor.compare(nodo->info) <= -1){
			EliminacionBalanceado(&(nodo->izq), BO, infor);
			Restructura1(&nodo, BO);
		} else {
			if (infor.compare(nodo->info) >= 1){
				EliminacionBalanceado(&(nodo->der), BO, infor);
				Restructura2(&nodo, BO);
			} else {
				otro = nodo;
				if (otro->der == NULL) {
					nodo = otro->izq;
					*BO = TRUE;
				} else {
					if (otro->izq==NULL) {
						nodo = otro->der;
						*BO = TRUE;
					} else {
						*BO = FALSE;
						//Se borra y se reestructura
						Borra(&(otro->izq), &otro, BO);
						Restructura1(&otro, BO);
						otro = NULL;
					}
				}
			}
		}
	} else {
		cout << "El nodo NO se encuentra en el árbol" << endl;
	}
	*nodocabeza = nodo;
}

void Arbol::crear_grafo(NODO *raiz){
	Grafo *g = new Grafo(raiz);
}
