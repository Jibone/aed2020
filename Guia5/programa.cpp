#include "Grafo.h"
using namespace std;

#define TRUE 1
#define FALSE 0

// Funcion menú con la cual se mostrará un menú en pantalla y recibirá las opciones y datos ingresadas por el usuario.
void Menu() {
	int opcion;
	//Se crean 2 elementos de tipo string, con uno ingresaremos datos, y 
	string elemento, elemento_nuevo;
	//Se crean booleanos para el momento de añadir/reemplazar/buscar elementos 
	bool se_elimina, se_inserta, existe;

	int inicio;

	//Se crea nuestro arbol
	Arbol *arbol = new Arbol();

	/* Se abre el archivo con las ids (es un archivo modificado con solo 1500 ids, ya que el programa no generaba correctamente
	la imagen al incluir unas cuantas (varias) mas, de esta manera tambien deja espacio para que el usuario ingrese una buena 
	cantidad de ids/elementos */
	ifstream archivo("ids.txt");
	string linea;
	
	system("clear");

	//Se crea nodo raiz
	NODO *raiz = NULL;

	//Aquí se le insertan las ids del .txt al arbol
	while(getline(archivo, linea)){
		inicio = FALSE;
		//Se inserta una por una bajo el nombre de linea, que va variando el elemento que contiene.
		arbol->InsercionBalanceado(raiz, &inicio, linea);
	}

	//Este ciclo estará en funcionamiento hasta que se escoja salir o cuando se escoja una opcion que no está.
	do{
		cout << "\n--------------------" << endl;
		cout << "1) Insertar ID" << endl;
		cout << "2) Buscar ID" << endl;
		cout << "3) Eliminar ID" << endl;
		cout << "4) Modificar ID" << endl;
		cout << "5) Generar grafo" << endl;
		cout << "0) Salir" << endl;
		cout << "Seleccione una opción: ";
		cin >> opcion;

		//Se utiliza un Switch para realizar las acciones correspondientes a la opcion escogida por el usuario.
		switch(opcion) {
			case 1:
			cout << "Ingresar elemento: ";
			cin >> elemento;
			inicio = FALSE;
			arbol->InsercionBalanceado(raiz, &inicio, elemento);
			break;

			case 2:
			cout << "Buscar elemento: ";
			cin >> elemento;
			arbol->Busqueda(raiz, elemento, existe);
			break;

			case 3:
			cout << "Eliminar elemento: ";
			cin >> elemento;
			inicio = FALSE;
			arbol->EliminacionBalanceado(&raiz, &inicio, elemento);
			break;

			case 4:
			cout << "Ingrese elemento a eliminar: ";
			cin >> elemento;
			/* Para modificar un elemento del arbol, el que se desea eliminar debe existir y el que se desea incluir no puede
			* estar incluido de antes. Para esto se utiliza 2 booleanos (se_inserta y se_elimina), utilizamos el metodo
			* Busqueda() el cual nos dirá si el elemento a eliminar está en el arbol (para ser eliminado) y volvemos a usar el mismo metodo
			* para el nuevo elemento, el cual al no ser encontrado en el arbol podrá ser ingresado ya que es un elemento nuevo. */
			arbol->Busqueda(raiz, elemento, se_elimina);
			//si se_elimina es true significa que el nodo a eliminar si está en el arbol
			if(se_elimina == true){
				cout << "Ingrese elemento para reemplazar: ";
				cin >> elemento_nuevo;
				arbol->Busqueda(raiz, elemento_nuevo, se_inserta);
				//Si se_inserta es false significa que el nuevo nodo no estaba en el arbol, por lo que será insertado.
				if(se_inserta == false){
					arbol->EliminacionBalanceado(&raiz, &inicio, elemento);
					arbol->InsercionBalanceado(raiz, &inicio, elemento_nuevo);
					cout << "Se ha modificado el elemento " << elemento << " por el elemento " << elemento_nuevo << endl;
				}else{
					cout << "No se han hecho cambios, debido a que el elemento a ingresar ya se encontraba en el arbol" << endl;
				}
			}else{
				cout << "El elemento que se desea modificar no existe en el arbol.." << endl;
			}
			break;

			case 5:
			arbol->crear_grafo(raiz);
			break;

			case 0:
			exit(1);
			break;
		}
	//Aquí se puede observar que el while tiene el condicionante de que el valor de opcion se encuentre entre 0 y menos que 6 para seguir funcionando 
	} while (opcion<6 && opcion>=0);
}

//Funcion main
int main(int argc, char **argv) {

	Menu();

	return 0;
}
