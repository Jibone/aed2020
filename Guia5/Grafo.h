#include "Arbol.h"
using namespace std;

//Clase Grafo
#ifndef GRAFO_H
#define GRAFO_H

class Grafo {
	private:

	public:
		//Constructor que comienza a generar el gráfico a partir de un nodo raíz que entra como parámetro :$
		Grafo(NODO *nodo);

		//Método que recorre el árbol desde un nodo entregado y va ingresando datos a nuestro archivo "grafo.txt"
		void recorrer(NODO *p, ofstream &archivo);
};
#endif
