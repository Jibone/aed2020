#include <iostream>
#include <fstream>
using namespace std;
//Clase Arbol
#ifndef ARBOL_H
#define ARBOL_H

typedef struct _NODO {
	struct _NODO *izq;
	struct _NODO *der;
	string info;
	int FE;
} NODO;

class Arbol{
	private:

	public:

		//Constructor
		Arbol();
		
		//Metodos
		//Inserta un nuevo elemento de forma balanceada
		void InsercionBalanceado(NODO *&nodo_nuevo, int *BO, string infor);
		//Busca un elemento específico en el arbol
		void Busqueda(NODO *nodo, string infor, bool &existe);
		//Reestructura hacia la izquierda
		void Restructura1(NODO **nodocabeza, int *BO);
		//Reestructura hacia la derecha
		void Restructura2(NODO **nodocabeza, int *BO);
		//Borra un nodo
		void Borra(NODO **aux1, NODO **otro1, int *BO);
		//Busca y elimina un nodo dejando balanceado el arbol
		void EliminacionBalanceado(NODO **nodocabeza, int *BO , string infor);
		//Genera el grafico
		void crear_grafo(NODO *raiz);
};
#endif
