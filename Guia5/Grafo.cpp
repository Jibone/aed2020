#include "Grafo.h"
using namespace std;

//Clase Grafo

/*Constructor de Grafo, recibe como parametro un nodo (la raíz del arbol) y abre genera un archivo "grafo.txt" el cual se irá
 * llenando con la información necesaria para generar el grafico. */
Grafo::Grafo(NODO *nodo){
	ofstream fp;
	//Se crea el archivo "grafo.txt" y se le comienza a añadir la información correspondiente, 
	fp.open("grafo.txt");
	fp << "digraph G {" << endl;
	fp << "node [style=filled fillcolor=cyan];" << endl;

	fp << "nullraiz [shape=point];" << endl;
	fp << "nullraiz->" << "\"" << nodo->info << "\"" << " " << "[label=" << nodo->FE << "];" << endl;
	//Utilizamos la funcion recorrer() para recorrer el arbol desde el nodo raíz que tenemos
	recorrer(nodo, fp);

	fp << "}";
	//Aquí se deja de añadir información al archivo y es cerrado.
	fp.close();
  
	//Con este comando generamos el gráfico a partir del "grafo.txt"
	system("dot -Tpng -ografo.png grafo.txt &");
  
	/*Con este comando abrimos el gráfico, sin embargo como éste se demora en generarse lo más probable es que suelte un error
	al abrir la imagen, pero puede volver a ingresar la opcion de generar grafico y esta vez será abierto el grafico generado anteriormente */
	system("eog grafo.png &");

}


//Metodo que recorre el arbol en preorden y va ingresando datos al archivo
void Grafo::recorrer(NODO *p, ofstream &fp){
	if (p != NULL) {
	//Se ingresan datos del nodo y su rama izquierda en "grafo.txt"
		if (p->izq != NULL) {
			fp << "\"" << p->info << "\"" << "->" << "\"" <<p->izq->info << "\"" << "[label=" <<p->izq->FE  << "];" << endl;
		}else{
			fp <<  "\"" << p->info << "i\"" << " [shape=point];" << endl;
			fp << "\"" <<p->info << "\"" << "->" << "\"" << p->info << "i\"" << ";" << endl;
		}
		//Se ingresan los datos de la rama derecha
		if (p->der != NULL) {
			fp << "\"" <<p->info << "\"" << "->" << "\"" << p->der->info << "\"" << "[label=" <<p->der->FE << "];" << endl;;
		} else{
			fp <<  "\"" << p->info << "d\""  << " [shape=point];" << endl;
			fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "d\"" << ";" << endl;
		}
   
   
	recorrer(p->izq, fp);
	recorrer(p->der, fp);
	}
}

