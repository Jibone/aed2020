#include <iostream>
#include "Pila.h"
using namespace std;
//Constructores, utilizaremos uno vacío () y otro con parametros (int n), para utilizar el que mas nos acomóde
Pila::Pila (){
}
Pila::Pila(int n){
	this->max = n;
	this->tope = -1;
	this->conts = new Contenedor[this->max];
}
//Con este metodo comprobamos si la pila está vacía.
void Pila::pila_vacia(){
	if(this->tope == -1){
		this->band = true;
		//la pila está vacía
	}
	else {
		this->band = false;
		//la pila NO está vacía
	}
}
//Con este metodo comprobamos si la pila está llena.
void Pila::pila_llena(){
	if(this->tope == this->max){
		this->band = true;
		//la pila está llena
	}
	else{
		this->band = false;
		//la pila NO está llena
	}
}

/*Con este metodo mandamos el dato que se quiere ingresar (en este caso este dato sería un objeto de la clase Contenedor).
  Este dato será el ultimo dato que haya sido ingresado en la pila.*/
void Pila::push(Contenedor cont){
	pila_llena();
	Contenedor dato = cont;
	if(this->band == true){
		cout << "Desbordamiento, Pila llena" << endl;
	}
	else{
		
			this->tope = this->tope + 1;
			this->conts[this->tope] = dato;
	}
	
}
//Con este metodo borramos el ultimo dato ingresado en la pila.
void Pila::pop(){
	Contenedor dato;
	pila_vacia();
	if(this->band == true){
		cout << "Subdesbordamiento, Pila vacía" << endl;
	}
	else{
		dato = this->conts[this->tope];
		this->conts[this->tope] = Contenedor();
		this->tope = this->tope - 1;
	}
}
//Con este otro metodo pop podremos eliminar contenedores que se encuentren debajo de otros, moviendo los que tenga encima.
void Pila::pop(int columna, int posicion, Pila *puerto){
	//Aquí comprueba que la posicion del contenedor que se le pide eliminar esté dentro del rango de contenedores que posee la pila.
	if(posicion <= 0 || posicion > this->tope+1){
		cout << "**La posición ingresada es inválida**" << endl;
	}
	/*En este else if; si resulta verdadera la comparacion significa que el dato a eliminar se encuentra encima en la pila
		y por esto pasa a ser eliminado sin necesidad de mover los demas contenedores*/
	else if(posicion == this->tope+1){
		Contenedor *contenedor;
		contenedor = this->conts;
		cout << "**El contenedor " << contenedor[posicion-1].get_empresa() << "/" << contenedor[posicion-1].get_codigo() << " ha sido eliminado**" << endl;
		pop();
		
	}
	//Y finalmente en este else caerían los contenedores a eliminar que tienen contenedores encima, por lo que estos ultimos tendrán que ser cambiados de lugar.
	else{
		Contenedor *contenedor;
		int i = 0;
		while(posicion-1 < this->tope){
		//	contenedores = this->pilas[i].get_pila();
			
			if(i != columna-1 && puerto[i].get_tope() < puerto[i].get_max()){
			//	tope = this->pilas[i].get_tope();
				contenedor = this->conts;
				puerto[i].push(contenedor[this->tope]);
				puerto[columna-1].pop();
			}
			i++;
			
		}
		puerto[columna-1].pop();
	}
}
//Metodo print, al cual le pasaremos el arreglo de Pilas para que pueda imprimirlo. Presenta dos for debido a que se imprimirá de forma similar a una matriz.
void Pila::print(Pila *puerto, int m){
	Contenedor *contenedor;
	for(int i = 1; i <= this->max ; i++){
		for(int j = 0; j <= m-1; j++){
			if(this->max - i >= puerto[j].get_tope()+1){
				cout << "[         ]";
			}
			else{
				contenedor = puerto[j].get_pila();
				cout << "[ " << contenedor[this->max - i].get_empresa() << "/" << contenedor[this->max - i].get_codigo() << " ]";
			}
		
		}
		cout << "" << endl;	
	}
}
int Pila::get_tope(){
	return this->tope;
}
Contenedor* Pila::get_pila(){
	return this->conts;

}
int Pila::get_max(){
	return this->max;
}
