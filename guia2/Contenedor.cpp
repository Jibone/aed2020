#include <iostream>
#include "Contenedor.h"
using namespace std;
//Se tienen 2 constructores para contenedor, debido a que hay casos en que uno o el otro pueden resultar más eficientes.
Contenedor::Contenedor(){
	this->empresa = "/n";
	this->codigo = 0;
}
Contenedor::Contenedor(string empresa, int codigo){
	this->empresa = empresa;
	this->codigo = codigo;
}
string Contenedor::get_empresa(){
	return this->empresa;
}
int Contenedor::get_codigo(){
	return this->codigo;
}

