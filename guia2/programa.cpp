#include <iostream>
#include "Pila.h"
using namespace std;
/*Aquí se crea una funcion menu() para llamar dentro de un ciclo que se estará ejecutando constantemente.
mientras estemos ejecutando el programa*/
void menu(){
	cout << "---------Menu----------" << endl;
	cout << "Agregar Contenedor  [1]" << endl;
	cout << "Remover Contenedor  [2]" << endl;
	cout << "Ver puerto          [3]" << endl;
	cout << "Salir               [4]" << endl;
	cout << "---------------------" << endl;
}
//Cuando se inicie el programa el usuario ingresara por consola los valores de m y n, que serían las pilas y su altura respectivamente.
int main(int argc, char *argv[]){
	int m = atoi(argv[1]);
	int n = atoi(argv[2]);
	int salir = 0;
	int opcion, codigo, columna, posicion;
	string empresa;
	cout << "-------Bienvenido al Software del Puerto-------" << endl;
	//Se crea nuestro array de pilas y se inicializa con el for que le sigue.
	Pila *puerto = new Pila[m-1];
	for(int i = 0; i < m; i++){
		puerto[i] = Pila(n);
	}
	//Creamos un ciclo dentro del cual se va a estar mientras el programa se esté ejecutando.
	while(salir != 1){
		cout << " " << endl;
		//Se imprime el menu y despues se le pide al usuario escoger una opcion del mismo ingresando el numero correspondiente.
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		//Cada if representa una opcion del menú.
		if(opcion == 1){
			cout << "¿En qué columna desea agregarlo? "; cin>>columna;
			cout << "Ingrese el nombre de la empresa dueña: "; cin>>empresa;
			cout << "Ingrese el codigo del contenedor: "; cin>>codigo;
			Contenedor nuevo = Contenedor(empresa, codigo);
			puerto[columna-1].push(nuevo);
			cout << "Contenedor agregado: [" << empresa << "/" << codigo << "]." << endl;
			cout << "" << endl;
			puerto[0].print(puerto, m);
		}
			
		if(opcion == 2){
			cout << "¿En qué columna se encuentra el contenedor a eliminar? "; cin>>columna;
			cout << "¿Y en qué posicion se encuentra? (de abajo hacia arriba) "; cin>>posicion;
			puerto[columna-1].pop(columna, posicion, puerto);
			cout << "**Dato Eliminado**" << endl;
			cout << "" << endl;
			puerto[0].print(puerto, m);
		}
		
		if(opcion == 3){
			puerto[0].print(puerto, m);
		}
			
		if(opcion == 4){
			salir = 1;
		}
		
	}
	cout << "¡Hasta pronto!" << endl;
	cout << " " << endl;
	
	return (0);
}
