# Software del Puerto.
Este programa organiza contenedores en pilas, los cuales pueden ir siendo agregados o eliminados.
Este programa recibe 2 parametros del tipo entero por terminal; m que sería la cantidad de pilas a crear, y n que sería la altura maxima de
contenedores aplicados que pueden haber.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa m n".
Una vez ejecutado el programa este desplegará un menú en pantalla, pidiéndole al usuario ingresar una opción.
Existe una opcion para agregar contenedores, otra para eliminar contenedores, otra para visualizar como lucen los contenedores en el puerto,
y finalmente está la opción para salir del programa.
De escoger agregar un contenedor;  el programa le pedirá al usuario que ingrese la columna en la cual desea agregarlo, el nombre de la empresa
dueña del contenedor, y el codigo de éste.
De escoger eliminar un contenedor; el programa preguntará en qué columna se encuentra y en qué posición. De haber contenedores encima del 
que se solicita eliminar, estos serán movidos a alguna pila con espacio disponible en un orden de izquierda a derecha.
El programa está escrito en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.


