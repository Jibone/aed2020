#include <iostream>
using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H
//Este es el archivo .h la clase Contenedor, en el tendremos los atributos (privados) y los constructores y metodos (publicos).
class Contenedor{
	
	private:
		string empresa;
		int codigo;
	
	public:
		Contenedor();
		Contenedor(string empresa, int codigo);
		string get_empresa();
		int get_codigo();
};
#endif
