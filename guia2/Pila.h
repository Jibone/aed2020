#include <iostream>
#include "Contenedor.h"
using namespace std;

#ifndef PILA_H
#define PILA_H
//Creamos la clase Pila en el archivo .H, declaramos sus atributos en privados, y constructores y metodos en publico.
class Pila {
	private:
		int max;
		int tope;
		bool band;
		Contenedor *conts;
		
	public:
	//Constructores, utilizaremos uno vacío () y otro con parametros (int n), para utilizar el que mas nos acomóde
		Pila ();
		Pila (int n);
		
		/*Metodos, estos son los métodos de la clase Pila, los cuales serán ocupados durante la ejecucion del programa*/
		void pila_vacia();
		void pila_llena();
		void push(Contenedor cont);
		void pop();
		void pop(int columna, int posicion, Pila *puerto);
		void print(Pila *puerto, int m);
		int get_tope();
		Contenedor* get_pila();
		int get_max();
};
#endif
