#include "Arreglomanager.h"

Arreglomanager::Arreglomanager(){}

int Arreglomanager::hash(int clave, int tamano) {
	// Función hash por módulo, asigna una posición para guardar el dato
	clave = (clave%(tamano - 1)) + 1;
	return clave;
}

void Arreglomanager::ingreso_lineal(int *arreglo, int tamano, int clave, int posicion){
	int nuevaPosicion;
	int cont;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) {
		// Revisa si el número ya existe en el arreglo 
		cout << " Clave en la posicion <" << posicion << ">" << endl;
	} else {
		cont = 0;
		nuevaPosicion = posicion + 1;

		while ( (nuevaPosicion != posicion)  && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion <= tamano) && (arreglo[nuevaPosicion] != clave) ){
			nuevaPosicion = nuevaPosicion + 1;
			if (nuevaPosicion == (tamano + 1)) {
				nuevaPosicion = 0; // Vuelve al inicio
			}
			cont++;
		}

		if (arreglo[nuevaPosicion] == '\0')	{
			// Posicion libre 
			arreglo[nuevaPosicion] = clave;
			cout << " Clave desplazada a la posición [" << "]" << endl;
		}

		if (cont == tamano) {
			cout << " Arreglo Lleno" << endl;
		}
	}
}

void Arreglomanager::ingreso_cuadratica(int *arreglo, int tamano, int clave, int posicion){
	int nuevaPosicion;
	int i; // Variable que aumentara cuadráticamente

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave) ) {
		cout << "Clave (" << clave << ") en la posicion [" << posicion << "]" << endl;
	} else {
		i = 0;
		// Avanza a la nueva posición con un incremento al cuadrado
		nuevaPosicion = (posicion + (i*i));

		while( (arreglo[nuevaPosicion] != '\0') && (arreglo[nuevaPosicion] != clave) ) { 
			//Revisa si la nueva posición esta vacia
			
			i++; //Aumenta para avanzar una posición
			nuevaPosicion = (posicion + (i*i)); //Genera una nueva posición

			if (nuevaPosicion > tamano) {
				i = 0;
				nuevaPosicion = 0; //Vuelve al inicio del arreglo
				posicion = 0;
			}
		}

		if (arreglo[nuevaPosicion] == '\0') {
			
			arreglo[nuevaPosicion] = clave;
			cout << " Clave (" << clave << ") desplazada a la posición <" << nuevaPosicion << ">" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion [" << nuevaPosicion << "]" << endl;
		}
	}
}

void Arreglomanager::ingreso_ddhash(int *arreglo, int tamano, int clave, int posicion){
	int nuevaPosicion;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave) ) {
		// Revisa si el número ya existe en el arreglo 
		cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
	} else {
		//Se le resta uno al tamaño porque el arreglo parte desde cero
		nuevaPosicion = ((posicion + 1)%tamano - 1) + 1;

		while ((nuevaPosicion <= tamano) && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != clave)){
			nuevaPosicion = ((nuevaPosicion + 1)%tamano - 1) + 1;
		}

		if (arreglo[nuevaPosicion] == '\0' or (arreglo[nuevaPosicion] != clave) ) {
			// Revisa si la nueva posición esta vacia
			arreglo[nuevaPosicion] = clave;
			if(nuevaPosicion == tamano + 1){
				cout << " EL arreglo está Lleno" << endl;
			} else { 
				cout << " Clave (" << clave << ") desplazada a la posición [" << nuevaPosicion << "]" << endl;
			}

		} else { 
				cout << " Clave (" << clave << ") en posicion [" << nuevaPosicion << "]" << endl;
		}
	}
}

void Arreglomanager::ingreso_encadenamiento(Nodo **arreglo, int *arreglo2, int tamano) {
	
	for (int i = 0; i < tamano; ++i) {
		arreglo[i] -> dato = arreglo2[i];
		arreglo[i] -> sig = NULL;
	}
}

void Arreglomanager::busqueda_lineal(int *arreglo, int tamano, int clave, int posicion){
	int nuevaPosicion;

	if ((arreglo[posicion] != '\0') && (arreglo[posicion] == clave) ) {
		cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
	} else {
		nuevaPosicion = posicion + 1;
		while ((arreglo[nuevaPosicion] != '\0') && (nuevaPosicion <= tamano) && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != clave)){
			nuevaPosicion = nuevaPosicion + 1;
			if (nuevaPosicion == tamano + 1) {
				nuevaPosicion = 0; //Vuelve al inicio
			}
		}
		if( (arreglo[nuevaPosicion] == '\0') or (nuevaPosicion == posicion)) {
			cout << " Clave (" << clave << ") no encontrado" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion [" << nuevaPosicion << "]" << endl;
		}
	}
}

void Arreglomanager::busqueda_cuadratica(int *arreglo, int tamano, int clave, int posicion){
	int nuevaPosicion;
	int i;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) { //Comprueba si el numero ya está en el arreglo
		cout << "Clave (" << clave << ") en la posicion <" << posicion << ">" << endl;
	} else {
		i = 1;
		// Avanza a la nueva posición con un aumento del cuadrado de la posicion anterior
		nuevaPosicion = (posicion + (i*i));
		while ( (arreglo[nuevaPosicion] != '\0') && (arreglo[nuevaPosicion] != clave)) {
			//Revisa si la nueva posición esta vacia
			i = i + 1; //Avanza una posición
			nuevaPosicion = (posicion + (i*i)); //acá se genera una nueva posición

			if(nuevaPosicion > tamano){ 
				i = 1;
				nuevaPosicion = 0; //Vuelve al inicio del arreglo
				posicion = 0;
			}
		}
		if (arreglo[nuevaPosicion] != '\0')	{
			cout << "Clave (" << clave << ") no encontrado" << endl;
		} else {
			cout << "Clave (" << clave << ") en posición [" << nuevaPosicion << "]" << endl;
		}
	}
}

void Arreglomanager::busqueda_ddhash(int *arreglo, int tamano, int clave, int posicion){
		int nuevaPosicion;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) { //Comprueba si el número ya está en el arreglo
		
		cout << "Clave (" << clave << ") en posicion [" << posicion << "]" << endl;
	} else {
		nuevaPosicion = ((posicion + 1)%tamano - 1) + 1;
		while ((nuevaPosicion <= tamano) && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != clave)) {
			//Resta uno al tamaño porque el arreglo parte desde cero
			nuevaPosicion = ((nuevaPosicion + 1)%tamano -1) + 1;
		}
		if ( (arreglo[nuevaPosicion] == '\0') || (nuevaPosicion == posicion) ) {
			cout << "Clave (" << clave << ") no encontrado" << endl;
		} else {
			cout << "Clave (" << clave << ") en posicion [" << nuevaPosicion << "]" << endl;
		}
	}
}

void Arreglomanager::busqueda_encadenamiento(int *arreglo, int tamano, int clave, int posicion){
	Nodo *temp = NULL;
	Nodo *arregloAux[tamano];

	for (int i = 0; i < tamano; ++i) {
		arregloAux[i] = new Nodo();
		arregloAux[i]->sig = NULL;
		arregloAux[i]->dato = '\0';

	}
	ingreso_encadenamiento(arregloAux, arreglo, tamano);

	if ((arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) {
		cout << " Clave (" << clave << ") en posicion [" << posicion << "]" << endl;
	} else {
		temp = arregloAux[posicion]->sig;
		while ((temp != NULL) && (temp->dato != clave)) {
			temp = temp->sig;
		}

		if (temp == NULL) {
			cout << " Clave (" << clave << ">) no encontrada en la Lista" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion [" << posicion << "]" << endl;
		}
	}
}





