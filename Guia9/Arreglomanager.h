#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

#ifndef ARREGLOMANAGER_H
#define ARREGLOMANAGER_H

//Estructura Nodo
typedef struct _Nodo{
	int dato;
	struct _Nodo *sig;
} Nodo;

class Arreglomanager {
	private:
	
	public:
		// Constructor 
		Arreglomanager();

		//Métodos para el ingreso de datos
		int hash(int clave, int tamano);
		void ingreso_lineal(int *arreglo, int tamano, int clave, int posicion);
		void ingreso_cuadratica(int *arreglo, int tamano, int clave, int posicion);
		void ingreso_ddhash(int *arreglo, int tamano, int clave, int posicion);
		void ingreso_encadenamiento(Nodo **arreglo, int *arreglo2, int tamano);

		//Métodos para la búsqueda de datos
		void busqueda_lineal(int *arreglo, int tamano, int clave, int posicion);
		void busqueda_cuadratica(int *arreglo, int tamano, int clave, int posicion);
		void busqueda_ddhash(int *arreglo, int tamano, int clave, int posicion);
		void busqueda_encadenamiento(int *arreglo, int tamano, int clave, int posicion);
	
};
#endif
