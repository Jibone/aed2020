#"Arreglo Con Tablas Hash"
El programa genera un arreglo al cual se le pueden ir ingresando datos los cuales serán ordenados de determinada
manera de acuerdo al método de resolución de colisiones ingresado por el usuario al ejecutar el programa.
Además se puede realizar la búsqueda específica de un elemento en el arreglo. Los metodos de resolución de colisiones son:
1) Reasignación de Prueba Lineal. (L)
2) Reasignación de Prueba Cuadrática. (C) 
3) Reasignación de Doble Dirección Hash. (D)
4) Encadenamiento. (E)
Se debe tener preinstalado el comando "make" para hacer uso de él mediante el archivo "makefile" con el cual se compila el programa.
Para iniciar el programa se debe encontrar dentro de la carpeta donde se están los archivos del programa a través de una terminal
de linux, una vez ahí compilar el programa con el comando "make" para luego ejecutarlo usando el comando "./programa [L/C/D/E]"
Donde el parametro [L/C/D/E] sería alguno de los métodos de resolución de colisiones mencionados anteriormente. De ingresar un parámetro
inválido se le informará al usuario y se le dirá qué opciones puede ingresar.
Una vez iniciado el programa le pedirá al usuario asginar el tamaño del arreglo para despues pasar al menú que nos ofrecerá 3 opciones:
1.Ingresar Número al arreglo, 2.Buscar Número dentro del arreglo, .3-Salir del programa.
El programa se encuentra escrito en el lenguaje de programacion C++.


