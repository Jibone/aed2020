#include "Arreglomanager.h"

using namespace std;

//Imprime arreglo
void imprimir(int arreglo[], int tamano) {
	cout << "Arreglo = {";
	for (int i = 0; i < tamano; ++i) {
		
		if (arreglo[i] == '\0') {
			cout << " * ";
		} else {
			cout << " " << arreglo[i] << " ";
		}
	}
	cout << "}" << endl;
}


// Comprueba el metodo para buscar dato en caso de colision */
void search(int *arreglo, int tamano, int dato, string metodo, int posicion) {
	Arreglomanager *hash = new Arreglomanager();

	if (metodo == "L") {
		cout << "-Metodo de Reasignación Prueba Lineal-" << endl;
		hash->busqueda_lineal(arreglo, tamano, dato, posicion);

	} else if (metodo == "C") {
		cout << "-Método de Reasignación Prueba Cuadrática-" << endl;
		hash->busqueda_cuadratica(arreglo, tamano, dato, posicion);

	} else if (metodo == "D") {
		cout << "-Método de Reasignación Doble Dirección Hash-" << endl;
		hash->busqueda_ddhash(arreglo, tamano, dato, posicion);

	}else if (metodo == "E") {
		cout << "-Método de Encadenamiento-" << endl;
		hash->busqueda_encadenamiento(arreglo, tamano, dato, posicion);
	}
	imprimir(arreglo, tamano);
}


void ingreso(int *arreglo, int tamano, int dato, string metodo, int posicion) {
	
	Arreglomanager *hash = new Arreglomanager();

	if (metodo == "L") {
		cout << "-Metodo de Reasignación Prueba Lineal-" << endl;
		hash->ingreso_lineal(arreglo, tamano, dato, posicion);

	} else if (metodo == "C") {
		cout << "-Método de Reasignación Prueba Cuadrática-" << endl;
		hash->ingreso_cuadratica(arreglo, tamano, dato, posicion);

	} else if (metodo == "D") {
		cout << "-Método de Reasignación Doble Dirección Hash-" << endl;
		hash->ingreso_ddhash(arreglo, tamano, dato, posicion);

	}else if (metodo == "E") {
		cout << "-Método de Encadenamiento-" << endl;
		hash->busqueda_encadenamiento(arreglo, tamano, dato, posicion);
	}
	imprimir(arreglo, tamano);
}

//Funcion que comprueba si el arreglo se encuentra vacío o no. (Vacío -> retorna true, no vacío retorna false)
bool estadoArreglo(int arreglo[], int tamano) {
	int cont = 0;
	for (int i = 0; i < tamano; ++i) { //Con este for se recorre el arreglo
		if (arreglo[i] == '\0') {
			
			cont++;
		}
	}
	if (cont == tamano) {
		
		return true;
		
	} else {
		
		return false;
	}
}

void arregloVacio(int *arreglo, int tamano) {
	// Llenar arreglo de ceros
	for (int i = 0; i < tamano; ++i) { //Con este for se recorre el arreglo
		arreglo[i] = '\0';
	}
}

void menu(string metodo, int *arreglo, int tamano){
	
	int opc;
	int dato;
	Arreglomanager *hash = new Arreglomanager();
	int posicion;

	do {

		cout << "" <<endl;
		cout << "Menu: " << endl;
		cout << "[1] Ingresar Número." << endl;
		cout << "[2] Buscar Número." << endl;
		cout << "[3] Salir" << endl;
		cout << "Elija una opción: "; 
		cin >> opc;

		switch(opc) {
			case 1:
				cout << " " << endl;
				
				cout << "Número a ingresar: ";
				cin >> dato;
				//Obtenemos la posicion utilizando hash
				posicion = hash->hash(dato, tamano);
				
				if (arreglo[posicion] == '\0') {
				 // Si la posicion está vacía se ingresa el dato
					arreglo[posicion] = dato;
					imprimir(arreglo, tamano);
				} else {
					//Si la posición está ocupada se produce una colisión
					cout << " Se produjo una colisión en la posición [" << posicion << "]" << endl;
					ingreso(arreglo, tamano, dato, metodo, posicion);
				}
				
				break;

			case 2:
				cout << " " << endl;
				cout << "Ingrese el número a buscar: ";
				cin >> dato;

				if (estadoArreglo(arreglo, tamano) == false) {
					//De no estar vacío el arreglo, nos encontraríamos dentro de este if
					posicion = hash->hash(dato, tamano);
					if (arreglo[posicion] == dato) {
						cout << " Clave (" << dato << ") en la posición [" << posicion << "]" << endl;
						imprimir(arreglo, tamano);
					} else {
						cout << " Colisión en la posicion [" << posicion << "]" << endl;
						//Revisa con que método se resolverá la colisión
						search(arreglo, tamano, dato, metodo, posicion);
					}
				} else {
				
					cout << "***El arreglo está vacío***" << endl;
					cout << " Dato:  " << dato << " no encontrado." << endl;
				}
				cout << "\n";
				break;

			case 3:
				exit(1);
				break;
		}
	} while (opc != 3);
}

int main(int argc, char *argv[]) {
	//Primero comprueba el ingreso de los argumentos correspondientes
	if (argc != 2) {
		cout << "*Debe ingresar un metodo de resolución de colisiones.*" << endl;
		cout << "Ejemplo: ./programa [L/C/D/E]" << endl;
		exit(1);

	} else {

		try {
			string metodo = argv[1]; //Este es el argumento ingresado [L/C/D/E] por el usuario
			
			if (metodo == "L") {
				cout << "-Metodo de Reasignación Prueba Lineal-" << endl;

			} else if (metodo == "C") {
				cout << "-Método de Reasignación Prueba Cuadrática-" << endl;

			} else if (metodo == "D") {
				cout << "-Método de Reasignación Doble Dirección Hash-" << endl;

			}else if (metodo == "E") {
				cout << "-Método de Encadenamiento-" << endl;

			} else {
				// Argumento invalido
				cout << "*Error, argumento inválido.*" << endl;
				cout << "*Debe ingresar un metodo de resolución de colisiones.*" << endl;
				cout << "Ejemplo: ./programa [L/C/D/E]" << endl;
				exit(1);
			}

			int tamano;
			cout << "Ingrese el tamaño del arreglo: ";
			cin >> tamano;
			int arreglo[tamano]; //Define el arreglo
			arregloVacio(arreglo, tamano); //Rellena el arreglo de 0
			imprimir(arreglo, tamano);

			//Muestra el menú y recibe las opciones y numeros ingresados al ser usado.
			menu(metodo, arreglo, tamano);

		} catch (const std::invalid_argument& e) {
			// Argumento invalido
			cout << "*Error, argumento inválido.*" << endl;
			cout << "*Debe ingresar un metodo de resolución de colisiones.*" << endl;
			cout << "Ejemplo: ./programa [L/C/D/E]" << endl;
			exit(1);
		}
	}
	return 0;
}
