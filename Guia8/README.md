#Generador de Graficos De Arboles binarios balanceados de IDs de Proteínas"
El programa busca generar un vector del tamaño que el usuario desee y que sea rellenado con valores generados aleatoriamente, 
además de imprimir en pantalla el tiempo que se demoró en ordenar ese mismo vector (en milisegundos) con 2 métodos de ordenamiento distintos (De selección 
o Quicksort). El usuario puede decidir si desea ver el contenido del vector y como quedan posteriormente a ser ordenado de ambas maneras.
Se debe tener preinstalado el comando "make" para hacer uso de él mediante el archivo "makefile" con el cual se compila el programa.
Para iniciar el programa se debe encontrar dentro de la carpeta donde se están los archivos del programa a través de una terminal
de linux, una vez ahí compilar el programa con el comando "make" para luego ejecutarlo usando el comando "./programa [numero] [s/n]"
el parámetro [numero] es el tamaño del vector que se desea crear (la cantidad de elementos que tendrá) y el parámetro [s/n] sería; si se
desea visualizar los elementos del vector y como quedan post-ordenamientos debería ingresar el carácter "s", y si no se desea visualizar
estos datos debería ingresar el carácter "n".
Si al momento de ejecutar el programa se encuentra algun error en los parámetros ingresados el mismo programa lo informará y le dirá
al usuario como debieran ser ingresados.
El programa está escrito en el lenguaje de programacion C++.


