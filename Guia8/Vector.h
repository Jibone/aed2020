#include <iostream>
#include <time.h>

using namespace std;

#ifndef VECTOR_H
#define VECTOR_H

//Clase Vector
class Vector{
	//Metodos privados
	private:
		void aux_quicksort(int ini, int fin, int &pos, int *vector);
	
	//Metodos publicos
	public:
		Vector();
		double quicksort(int *vector, int n);
		double seleccion(int *vector, int n);
		void generar_ordenar(int n, string arg);
		void imprimir_tiempos(double *tiempo, string *metodo);
};
#endif

