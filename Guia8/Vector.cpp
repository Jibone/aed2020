#include "Vector.h"
using namespace std;

Vector::Vector () {}
//Metodos
//
void Vector::aux_quicksort(int ini, int fin, int &pos, int *vector){

	int aux;
	int izq = ini;
	int der = fin;
	bool sorting = 1;
	pos = ini;

	while(sorting){
		while(vector[pos] <= vector[der] && pos != der){
			der--;
		}
		if(pos == der){
			sorting = 0;
		}
		else{
			aux = vector[pos];
			vector[pos] = vector[der];
			vector[der] = aux;
			pos = der;

			while(vector[pos] >= vector[izq] && pos != izq){
				izq++;
				aux = vector[pos];
				vector[pos] = vector[izq];
				vector[izq] = aux;
				pos = izq;
			}
		}
	}
}

// Metodo de ordenamiento Quicksort, que retorna el tiempo que se demoró en ordenar los datos del vector.
double Vector::quicksort(int *vector, int n){

	clock_t inicio = clock();
	int pilamenor[n];
	int pilamayor[n];
	int ini;
	int fin;
	int pos;
	int tope = 0;
	pilamenor[tope] = 0;
	pilamayor[tope] = n-1;

	while(tope >=0 ){
		ini = pilamenor[tope];
		fin = pilamayor[tope];
		tope--;
		this->aux_quicksort(ini, fin, pos, vector);

		if(ini < (pos-1)){
			tope++;
			pilamenor[tope] = ini;
			pilamayor[tope] = pos-1;
		}
		if(fin > (pos+1)){
			tope++;
			pilamenor[tope] = pos+1;
			pilamayor[tope] = fin;
		}
	}
	return (double)(clock()-inicio)/CLOCKS_PER_SEC * 1000;
}

// Método de ordenamiento de Selección, que retorna el tiempo que se demoró en ordenar los datos del vector.
double Vector::seleccion(int *array, int n){

	clock_t inicio = clock();
	int menor;
	int k;

	for(int i=0; i<=(n-1); i++){

		menor = array[i];
		k = i;

		for(int j=(i+1); j<n; j++){
			if(array[j] < menor){
				menor = array[j];
				k = j;
			}
		}
		array[k] = array[i];
		array[i] = menor;
	}
	return (double)(clock()-inicio)/CLOCKS_PER_SEC * 1000;
}

void Vector::generar_ordenar(int n, string arg){

	Vector vector_quick;
	Vector vector_sel;
	int random;
	int vector[3][n];
	double tiempo[2];
	srand(time(0));

	for(int i=0; i<n; i++){
		random = rand()%n + 1;

		for(int j=0; j<3; j++){
			vector[j][i] = random;
		}
	}

	tiempo[0] = vector_sel.seleccion(vector[0], n);
	tiempo[1] = vector_quick.quicksort(vector[1], n);

	string metodo[2] = {"Seleccion", "Quicksort"};

	// Llamado para imprimir tiempos de ejecución por pantalla
	imprimir_tiempos(tiempo, metodo);

	if(arg == "s"){
		cout << "\n> Mostrando contenido del vector principal...";
		cout << "\n==================================================" << endl;
		cout << "\t[ VECTOR PRINCIPAL ]" << endl << "\n| ";

		for(int i=0; i<n; i++){
			cout << vector[2][i] << " > ";
		}
		cout << "|" << endl;
		cout << "==================================================" << endl;

		for(int i=0; i<2; i++){
			cout << metodo[i] << string(21-metodo[i].length(), ' ') << "|";

			for(int j=0; j<n; j++){
				cout << vector[i][j] << "|";
			}
			cout << endl;
		}
	}
}

// Función para imprimir tiempos finales de ejecución
void Vector::imprimir_tiempos(double *tiempo, string *metodo){

	cout << "\n\t [ METODOS DE ORDENAMIENTO ] " << endl;
	cout << "==================================================" << endl;
	cout << "  Método             |  Tiempo Utilizado (ms)" << endl;
	cout << "==================================================" << endl;

	for(int i=0; i<2; i++){
		cout << metodo[i] << string(21-metodo[i].length(), ' ') << "|" << "  " << tiempo[i] << " [ms]"<< endl;
	}
	cout << "__________________________________________________" << endl;
}



