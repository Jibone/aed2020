#include "Vector.h"
using namespace std;

/* Programa principal, primero se comprueban los parametros ingresados y si no están ingresandos
correctamente el programa le informará al usuario su error y le dirá como debe ser */

int main(int argc, char **argv){

	int n;
	string arg;
	Vector vector;

	if (argc != 3){
		cout << "*Debe ingresar 2 parámetros (./programa [numero] [s/n])*" << endl;
	}
	else{
		try{
			n = stoi(argv[1]);
			arg = argv[2];

			if(n > 1000000 || n < 1){
				cout << "*Ingrese un numero válido (Entre 1 y 1.000.000)";
				cout << "Ejemplo: ./programa [numero] [s/n]" << endl;
				exit(1);
			}
			else if (arg != "n" && arg != "s"){
				cout << "Ingrese un parámetro válido ([s/n])" << endl;
				cout << "Ejemplo: ./programa [numero] [s/n]" << endl;
				exit(1);
			}
		}
		catch (const std::invalid_argument& e){
			cout << "Error. Debe ingresar un número en el primer parámetro." << endl;
			cout << "Ejemplo: ./programa [numero] [s/n]" << endl;
			exit(1);
		}
		//Se llama a la funcion para generar y ordenar el vector, además de calcular cuanto tiempo demora el ordenamiento respectivo
		vector.generar_ordenar(n, arg);
	}

	return 0;
}

