#include <iostream>
using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

class Arreglo {
	//aqui declaramos un puntero del tipo string para crear el arreglo de frases
	private:
		int tamano;
		string *frases;
	
	public:
	/*hacemos un constructor vacío () y otro con parametros, así se nos hace mas practico
	para cuando nos acomode uno o el otro*/ 
	//Constructores
		Arreglo ();
		Arreglo (int);
		//Metodos
		void set_tamano(int);
		void rellenar_frases();
		void contador();
};
#endif
