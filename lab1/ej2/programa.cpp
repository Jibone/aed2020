#include <iostream>
#include <ctype.h>	
#include <string>
//#include <bits/stdc++.h> 
#include "Arreglo.h"
using namespace std;

int main(){
	int tamano;
	cout << "Contador de Mayusculas y Minusculas" << endl;
	//Hacemos que el usuario defina el tamaño del arreglo
	cout << "¿Cuantas frases desea ingresar?" << endl;
	cin >> tamano;
	cin.ignore();
	/*Se crea un Arreglo, el cual tiene como atributo un arreglo de palabras (string), el cual va
	ir siendo rellenado y para despues contar cuantas mayusculas y minusculas tiene cada frase ingresada */
	Arreglo arreglo = Arreglo(tamano);
	arreglo.rellenar_frases();
	arreglo.contador();
}
