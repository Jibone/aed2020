# Contador de Mayúsculas y Minúsculas.
El programa está escrito en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez dentro deberá ingresar cuantas frases desea introducir.
Después tendrá que ir introduciendo cada frase (puede ser una palabra o más).
Posteriormente el programa mostrará en pantalla las frases introducidas, y la cantidad de minúsculas y mayúsculas que tiene cada una.

