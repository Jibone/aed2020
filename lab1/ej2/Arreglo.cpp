#include <iostream>
#include "Arreglo.h"
using namespace std;

/*hacemos un constructor vacío () y otro con parametros,
así se nos hace mas práctico para cuando nos acomode uno o el otro */
Arreglo::Arreglo () {
}

Arreglo::Arreglo(int tamano){
	this->tamano = tamano;
	this->frases = new string[this->tamano];
}
//Con este metodo recorremos el arreglo y vamos introduciendo una frase por cada posicion. (Así es, una frase, no tan solo una palabra)
void Arreglo::rellenar_frases(){
	string frase;
	for(int i = 0; i < this->tamano; i++){
		cout << "Ingrese la frase numero " << i+1 << ": " << endl;
		//cin.ignore();
		getline(cin, frase);
		this->frases[i] = frase;
	}
}
/*Este metodo va recorriendo el arreglo de frases, y por cada posicion va recorriendo la frase letra por letra
viendo si es mayuscula o minuscula, contandolas para despues decirte cuantas tenía cada frase.*/
void Arreglo::contador(){
	int largo, suma_minus=0, suma_mayus=0;
	for(int i = 0; i < this->tamano; i++){
		string palabra = this->frases[i];
		largo = palabra.length();
		char str[largo+1];
		for (int i = 0; i<largo ; i++){
			str[i] = palabra[i];
			if (islower(str[i])){
				suma_minus++;
			}
			if (isupper(str[i])){
				suma_mayus++;
			}
		}
		cout << "La frase: " << frases[i] << "." << endl;
		cout << "Tiene " << suma_minus << " letras minusculas y " << suma_mayus << " letras mayusculas." <<  endl;
		cout << " " << endl;
		suma_minus = 0;
		suma_mayus = 0;
	}
}


