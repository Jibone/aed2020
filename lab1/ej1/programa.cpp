#include <iostream>
#include "Arreglo.h"

using namespace std;

int main(void){
	int tamano;
	cout << "**************************************************************" << endl;
	cout << "*********** Bienvenido a la Calculadora de la suma  **********" << endl;
	cout << "*********** del cuadrado de los numeros ingresados  **********" << endl;
	cout << "**************************************************************" << endl;
	//Le pedimos al usuario que defina el tamaño del arreglo
	cout << "Ingrese el tamaño de su arreglo: " << endl; 
	cin >> tamano;
	//Despues lo inicializamos y hacemos uso de sus metodos para introducir los datos, imprimirlos, y despues calcular la suma del cuadrado.
	Arreglo arreglo = Arreglo(tamano);
	arreglo.rellenar_vector();
	arreglo.imprimir_vector();
	cout << "El resultado de la suma del cuadrado de los datos ingresados es: " << arreglo.suma_cuadrados() << endl;
	//cout << "despues print suma" << endl;
	
	exit(0);
}
