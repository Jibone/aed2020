#include <iostream>
using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

class Arreglo {
	private:
		int tamano;
		int vector[];
	
	public:
		/*Constructores; hacemos un constructor vacío () y otro con parametros, así se nos hace mas practico
		 para cuando nos acomode uno o el otro */
		Arreglo();
		Arreglo(int);
		/* Metodos */
		void set_tamano(int);
		void rellenar_vector();
		void imprimir_vector();
		int suma_cuadrados();
		int get_tamano();
		
		

};
#endif
