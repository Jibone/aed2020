# Calculadora del cuadrado de los datos ingresados.
El programa está escrito en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez dentro deberá ingresar el tamaño del arreglo que va a ingresar.
Después tendrá que ir ingresando los valores de cada posición del arreglo, luego estos serán impresos en pantalla
y al lado el cuadrado de estos. Para finalmente entregar la suma total del cuadrado de los valores ingresados.

