#include <iostream>
#include "Arreglo.h"
using namespace std;

/*Hacemos un constructor vacío () y otro con parametros, así se nos hace mas práctico
para cuando nos acomode uno o el otro */
Arreglo::Arreglo () {
}
Arreglo::Arreglo(int tamano){
	this->tamano = tamano;
}

void Arreglo::set_tamano(int tamano){
	this->tamano = tamano;
}

//Con este metodo vamos recorriendo el arreglo y le vamos asignando valores.
void Arreglo::rellenar_vector(){
	int aux;
	for(int i = 0; i< this->tamano; i++){
		cout << "Ingrese numero en la posicion: [" << i+1 << "]"<<endl;
		cin >> aux;
		this->vector[i] = aux;
	}
}

/*Con este metodo recorremos el arreglo e imprimimos sus valores y en la misma 
linea se hace calculo de el cuadrado de los valores */
void Arreglo::imprimir_vector(){
	cout << "Los datos del arreglo son: " << endl;
	for(int i = 0; i < this->tamano; i++){
		cout << "Arreglo[" << i << "]: " << vector[i] <<". Y su cuadrado es: " << vector[i]*vector[i] << endl;
	}
}
//Este metodo suma los cuadrados de los valores del arreglo.
int Arreglo::suma_cuadrados(){
	int suma = 0;
	int aux = 0;
	//cout << "Antes for" << endl;
	for(int i = 0; i < this->tamano; i++){
		aux = vector[i];
		suma = suma + (aux)*aux;
	}
	return suma;
}	

int Arreglo::get_tamano(){
	return this->tamano;
}
