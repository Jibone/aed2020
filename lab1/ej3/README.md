# Client Manager.
El programa está escrito en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez dentro el programa el programa le solicitará ingresar la cantidad de personas que desea registrar.
Posterior a esto le irá preguntando los datos de cada una de estas persona, nombre, teléfono, saldo y si es moroso (en este punto usted
tan solo debe ingresar "si" o "no).
Una vez ingresados los datos de todas las personas el programa va a imprimir de vuelta estos mismos datos ordenados.


