#include <iostream>
#include "Cliente.h"
using namespace std;
/*hacemos un constructor vacío () y otro con parametros,
así se nos hace mas práctico para cuando nos acomode uno o el otro */
Cliente::Cliente() {
}

Cliente::Cliente(string nombre, string telefono, int saldo, bool mora){
	this->nombre = nombre;
	this->telefono = telefono;
	this->saldo = saldo;
	this->mora = mora;
}
//Con este metodo se van solicitando y rellenando los datos del Cliente.
void Cliente::rellenar_datos(){
	string respuesta;
	cout << "Ingrese el nombre:"<< endl;
	cin.ignore();
	getline(cin, this->nombre);
	cout << "Ingrese el numero de telefono:" << endl;
	cin >> this->telefono;
	cout << "Ingrese el saldo ($):" << endl;
	cin >> this->saldo;
	cout << "¿El cliente debe? (si/no):" << endl;
	cin >> respuesta;
	if(respuesta == "si"){
		this->mora = true;
	}
	if(respuesta == "no"){
		this->mora = false;
	}
	
}

void Cliente::set_nombre(string nombre){
	this->nombre = nombre;
}
void Cliente::set_telefono(string telefono){
	this->telefono = telefono;
}
void Cliente::set_saldo(int saldo){
	this->saldo = saldo;
}
void Cliente::set_mora(bool mora){
	this->mora = mora;
}
//Con este metodo se van a imprimir los datos del Cliente.
void Cliente::imprimir_datos(){
	cout << "Nombre: " << this->nombre << endl;
	cout << "Telefono: " << this->telefono << endl;
	cout << "Saldo: $" << this->saldo << endl;
	cout << "Deuda por mora: ";
	if(this->mora == false){
		cout << "No." << endl;
	}
	if(this->mora == true){
		cout << "Si." << endl;
	}  
}
string Cliente::get_nombre(){
	return this->nombre;
}
string Cliente::get_telefono(){
	return this->telefono;
}
int Cliente::get_saldo(){
	return this->saldo;
}
void Cliente::get_mora(){
	if(this->mora == false){
		cout << "No" << endl;
	}
	if(this->mora == true){
		cout << "Si" << endl;
	}
}
	
