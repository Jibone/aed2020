#include <iostream>
#include "Cliente.h"
using namespace std;


int main(){
	int n;
	cout << "--------Client Manager--------" << endl;
	cout << "Ingrese la cantidad de clientes a registrar" << endl;
	cin >> n;
	//Hacemos que el usuario defina el tamaño del arreglo de clase Cliente
	Cliente *clientes = new Cliente[n];
	//Con este ciclo vamos a ir recorriendo el arreglo e introduciendole los datos correspondientes a cada Cliente.
	for(int i = 0; i < n; i++){
		cout << " " << endl;
		cout << "Cliente " << i+1 << ":" << endl;
		clientes[i].rellenar_datos();
		
	}
	//Con este ciclo se recorre el arreglo y va imprimiendo los datos del cliente que se encuentre en cada posicion.
	for(int i = 0; i < n; i++){
		cout << " " << endl;
		cout << "Datos del Cliente " << i+1 << endl; 
		clientes[i].imprimir_datos();
	}
	
	
}
