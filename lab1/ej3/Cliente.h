#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente {
	private:
		string nombre;
		string telefono;
		int saldo;
		bool mora;
		
	public:
		/*Constructores; hacemos un constructor vacío () y otro con parametros, así se nos hace mas practico
		 para cuando nos acomode uno o el otro */
		Cliente();
		Cliente(string, string, int, bool);
		//Metodos
		void rellenar_datos();
		void set_nombre(string);
		void set_telefono(string);
		void set_saldo(int);
		void set_mora(bool);
		void imprimir_datos();
		string get_nombre();
		string get_telefono();
		int get_saldo();
		void get_mora();
};
#endif
