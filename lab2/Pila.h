#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H
//Creamos la clase Pila en el archivo .H, declaramos sus atributos en privados, y constructores y metodos en publico
class Pila {
	private:
		int max;
		int tope;
		bool band;
		int arreglo[];
		
	public:
	//Constructores, utilizaremos uno vacío () y otro con parametros (int max), para utilizar el que mas nos acomóde
		Pila ();
		Pila (int max);
		
		/*Para este punto se decidió utilizar un método con menos parámetros y otro con más, para poder pasarles
		  todos los parámetros que se pedían en los metodos que subió de ejemplo el profe en pseudocodigo en el mismo lab2.pdf */
		void pila_vacia(int tope, bool &band);
		void pila_llena(int tope, int max, bool &band);
		void push(int dato);
		void push(int *arreglo, int &tope, int max, int dato);
		void pop();
		void pop(int *arreglo, int &tope);
		void print();
};
#endif
