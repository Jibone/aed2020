#include <iostream>
#include "Pila.h"
using namespace std;

//Creamos una función llamada menú que contiene una especie de menú para utilizarla cada vez que se necesite desplegar este menú en pantalla
void menu(){
	cout << "--------Menu---------" << endl;
	cout << "Agregar/Push dato [1]" << endl;
	cout << "Remover/Pop dato  [2]" << endl;
	cout << "Ver pila          [3]" << endl;
	cout << "Salir             [4]" << endl;
	cout << "---------------------" << endl;
}
int main(){
	int max;
	int salir = 0;
	int opcion;
	int dato;
	//Hacemos que el usuario ingrese la cantidad máxima de elementos que tendrá la pila
	cout << "-------Bienvenido a la Pila-------"
	cout << "¿Cantidad maxima de datos en la pila?: "; cin>>max;
	Pila pila = Pila(max);
	//Creamos un ciclo dentro del cual se va a estar mientras el programa se esté ejecutando
	while(salir != 1){
		cout << " " << endl;
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		/*Se le pide al usuario que ingrese una opcion de las del menú ingresando el numero correspondiente, utilizandose este mismo
		numero para escoger un caso dentro de este switch*/
		switch(opcion){
			case 1:
				cout << "Ingrese el dato a agregar: "; cin>>dato;	
				pila.push(dato);
				break;
			case 2:
				pila.pop();
				break;
			case 3:
				pila.print();
				break;
			case 4:
				salir = 1;
				break;
		}
		
	}
	cout << "¡Hasta pronto!" << endl;
	cout << " " << endl;
	
	return (0);
}
	

