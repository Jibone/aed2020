#include <iostream>
#include "Pila.h"
using namespace std;

Pila::Pila (){
}
Pila::Pila(int max){
	this->max = max;
	this->tope = 0;
	//this->arreglo = new int[max];
}
//Con este metodo comprobamos si la pila está vacía
void Pila::pila_vacia(int tope, bool &band){
	if(tope == 0){
		band = true;
		//la pila está vacía
	}
	else {
		band = false;
		//la pila NO está vacía
	}
}
//Con este metodo comprobamos si la pila está llena
void Pila::pila_llena(int tope, int max, bool &band){
	if(tope == max){
		band = true;
		//la pila está llena
	}
	else{
		band = false;
		//la pila NO está llena
	}
}
//Con este metodo mandamos el dato que se quiere ingresar y despues se manda a otro metodo push pero con todos los parametros que este en verdad solicita.
//este dato será el ultimo dato ingresado en la pila.
void Pila::push(int dato){
	push(this->arreglo, this->tope, this->max, dato);
}
void Pila::push(int *arreglo, int &tope, int max, int dato){
	pila_llena(tope, max, this->band);
	if(this->band == true){
		cout << "Desbordamiento, Pila llena" << endl;
	}
	else{
		tope = tope +1;
		arreglo[tope] = dato;
		cout << "Elemento agregado: " << dato << endl;
		cout << arreglo[tope];
	}
}
//Con este metodo borramos el ultimo dato ingresado en la pila
void Pila::pop(){
	pop(this->arreglo, this->tope);
}
void Pila::pop(int *arreglo, int &tope){
	int dato;
	pila_vacia(tope, this->band);
	if(this->band == true){
		cout << "Subdesbordamiento, Pila vacía" << endl;
	}
	else{
		dato = arreglo[tope];
		arreglo[tope] = 0;
		tope = tope - 1;
		cout  << "Elemento eliminado: " << dato << endl;
	}
}
//Con este metodo imprimimos la pila desde el ultimo dato ingresado, hasta el primero.
void Pila::print(){

	for(int i = 0; i < this->tope; i++){
		cout << "|" << this->arreglo[this->tope - i] << "|" << endl;
	}
}
