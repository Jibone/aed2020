# Pila (en arreglo).
El programa está escrito en el lenguaje de programación C++, y para su uso se debe encontrar en una distribución de linux.
Para iniciar el programa se debe hacer desde la terminal, primero compilando desde dentro de la carpeta donde se encuentra el archivo makefile,
utilizando el comando "make", para luego iniciarlo con "./programa".
Una vez dentro deberá ingresar el tamaño de la pila que se quuiere crear.
Despues aparecerá en pantalla un menú con un numero para cada opción, el usuario debe ingresar el numero correspondiente a la opción que desee,
de querer agregar un elemento se le solicitará que ingrese el numero que desea añadir, si la pila está llena no lo añadirá y se enviará un mensaje informando esto.
Si escoge eliminar un elemento, se eliminará el último elemento ingresado en la pila, si no hay mas elementos se enviará un mensaje informando y no se hará nada.
Si escoge imprimir la pila se mostrarán en pantalla los elementos que esta contiene desde el ultimo ingresado hasta el primero.
Si escoge salir; el programa finaliza.

