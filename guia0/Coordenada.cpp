#include <iostream>
#include "Coordenada.h"
using namespace std;

Coordenada::Coordenada (){}

Coordenada::Coordenada(float x, float y, float z){
	this->x = x;
	this->z = z;
	this->y = y;
}
void Coordenada::set_x(float x){
	this->x = x;
}

void Coordenada::set_y(float y){
	this->y = y;
}

void Coordenada::set_z(float z){
	this->z = z;
}

float Coordenada::get_x(){
	return this->x;
}

float Coordenada::get_y(){
	return this->y;
}

float Coordenada::get_z(){
	return this->z;
}
